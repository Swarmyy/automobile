"""
"""
import csv
import os
import datetime

"""
params:
    time: string a convertir
Converti time du format YY-MM-DD au format YY/MM/DD
rtn:
    On retourne la string convertie
"""


def setTime(time):    
    return datetime.datetime.strptime(time, '%Y-%m-%d').strftime('%Y/%m/%d')
    
"""
params:
    newOrder: dictionnaire qui contient la relation entre le Premier et Nouvel Ordre du .csv
    oldClassification: la vieille classification du .csv     
    row: la ligne du .csv étudiée
    firstOrder: l'ordre du .csv avant son changement
    isTypeVarianteVersion: boolean, False par défaut, qui dit si on est passé par le champ type_variante_version

rtn:
    On retourne la valeur "" s'il ne s'est rien passé, et isTypeVarianteVersion
"""


def setIndex(newOrder, oldClassification, row, firstOrder, isTypeVarianteVersion=False):
    newRow = ""
    if(newOrder.get(oldClassification) == "date_immat"):
        oldTime = row[firstOrder.index(newOrder.get(oldClassification))]
        newRow = setTime(oldTime)
    else:
        if(newOrder.get(oldClassification) != "type_variante_version"):
            newRow = row[firstOrder.index(newOrder.get(oldClassification))]
        else:
            if(isTypeVarianteVersion == False):
                isTypeVarianteVersion = True
                for data in row[firstOrder.index(newOrder.get(oldClassification))].split(','):
                    newRow = data
    return (newRow, isTypeVarianteVersion)


"""
params:
    reader: le fichier.csv sous forme d'une liste de listes
    newOrder: dictionnaire qui contient la relation entre le Premier et Nouvel Ordre du .csv
rtn: le fichier.csv modifié selon la nouvelle configuration spécifiée par le dictionnaire newOrder
"""


def transformCSV(dataRows, newOrder):
    # for ref
    firstOrderCSV = 'address|carrosserie|categorie|couleur|cylindree|date_immat|denomination|energy|firstname|immat|marque|name|places|poids|puissance|type_variante_version|vin'
    #
    firstOrder = dataRows[0]
    newCSV = []
    newRefs = []
    for oldClassification in newOrder:
        newRefs.append(oldClassification)
    newCSV.append(newRefs)
    readerIndex = 0
    for row in dataRows:
        if readerIndex != 0:
            newRow = []
            isTypeVarianteVersion = False
            for oldClassification in newOrder:
                tmpNewRow, isTypeVarianteVersion = setIndex(
                    newOrder, oldClassification, row, firstOrder, isTypeVarianteVersion)
                if tmpNewRow != "":
                    newRow.append(tmpNewRow)
            newCSV.append(newRow)
        readerIndex += 1
    return newCSV


"""
params:
    file: fichier dans lequel on extrait le fichier.csv
    newOrder: dictionnaire qui contient la relation entre le Premier et Nouvel Ordre du .csv

rtn:
"""


def verifyFileAndTransformCSV(file, thisdelimiter='|'):
    # new:old
    newOrder = {'adresse_titulaire': 'address', 'nom': 'name', 'prenom': 'firstname', 'immatriculation': 'immat', 'date_immatriculation': 'date_immat', 'vin': 'vin', 'marque': 'marque', 'denomination_commerciale': 'denomination', 'couleur': 'couleur',
                'carrosserie': 'carrosserie', 'categorie': 'categorie', 'cylindree': 'cylindree', 'energie': 'energy', 'places': 'places', 'poids': 'poids', 'puissance': 'puissance', 'type': 'type_variante_version', 'variante': 'type_variante_version', 'version': 'type_variante_version'}
    if(os.path.exists(file)):
        if(os.path.isfile(file)):
            dataRows = getCSV(file)
            newCSV = transformCSV(dataRows, newOrder)
        else:
            return "Le fichier n'existe pas"
    else:
        return "Mauvais chemin de fichier"
    return newCSV


"""
params:
    file: le fichier à lire
    thisdelimiter:le délimiteur entre les données
Lis file, et met dans une liste de listes les composantes du fichier
rtn: retourne la liste de listes composée de tout les éléments du CSV
"""


def getCSV(file, thisdelimiter='|'):
    with open(file) as csvfile:
        reader = csv.reader(csvfile, delimiter=thisdelimiter)
        dataRows = []
        for row in reader:
            dataRows.append(row)
        return dataRows


"""
params:
    newCSV: la liste de liste étant le nouveau fichier .csv
    file: le fichier dans lequel on écrit, par défaut 'newauto.csv'
    thisdelimiter: le délimteur de chaque donnée du fichier, pardéfaut ';' 
Ecris dans le fichier spécifié file, avec délimiteur spécifié, à partir de newCSV
rtn: rien
"""


def writeCSV(newCSV, file, thisdelimiter=';'):
    with open(file, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=thisdelimiter)
        for row in newCSV:
            writer.writerow(row)


"""
Main
"""
file = 'auto.csv'
newCSV = verifyFileAndTransformCSV(file)
writeCSV(newCSV, "new"+file)
