import unittest
import csv

from CSVAuto import setTime
from CSVAuto import verifyFileAndTransformCSV

class TestCSVAuto(unittest.TestCase):        
    def test_setTime(self):
        self.assertEqual(setTime("1945-12-30"),"1945/12/30")        

    def test_verifyFileAndTransformCSV(self):
        file="./Test.csv"
        self.assertEqual(verifyFileAndTransformCSV("salut.csv"),"Mauvais chemin de fichier")
        self.assertEqual(type(verifyFileAndTransformCSV(file)),list)
        